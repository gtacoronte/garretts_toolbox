<?php

/**
* A PHP Toolbox of useful functions and methods.
* @author Garrett Tacoronte
* @created 7/23/15
* @updated 1/30/16
*/

/**
 * Pass in a url
 * pass in true if you want to check to see if the url will return a 404. else 
 * if it is an asset to a page, pass in false
 * myCurl will initialize cURL 
 * Set cURL's options using the previously assigned array data in $options
 * Execute the cURL request and assign the returned data to the $data variable
 * Then close cURL
 * 
 * @param string $url
 * @return string
 */

function myCurl($url, $status_check=false) {
    
    // Assigning cURL options to an array
    $options = [CURLOPT_RETURNTRANSFER => TRUE,
        
        // Setting cURL to follow 'location' HTTP headers
        CURLOPT_FOLLOWLOCATION => TRUE,
        
        // Automatically set the referer where following 'location' HTTP headers
        CURLOPT_AUTOREFERER => TRUE, 
        
        // Automatically set the referer where following 'location' HTTP headers
        CURLOPT_CONNECTTIMEOUT => 320,
        
        // Setting the maximum amount of time for cURL to execute queries
        CURLOPT_TIMEOUT => 320,
        // Setting the maximum number of redirections to follow
        CURLOPT_MAXREDIRS => 20,
        
        // Setting the useragent
        CURLOPT_USERAGENT => "Mozilla/5.0 (X11; U; Linux i686; en-US;"
        . "rv:1.9.1a2pre) Gecko/2008073000 Shredder/3.0a2pre ThunderBrowse/3.2.1.8",
        
        // Setting cURL's URL option with the $url variable passed into the function
        CURLOPT_URL => $url, 
        ];
    
    // Initializing cURL
    $ch = curl_init();
    
    // Setting cURL's options using the previously assigned array data in $options
    curl_setopt_array($ch, $options);
    
    // Executing the cURL request and assigning the returned data to the $data variable
    $data = curl_exec($ch);
    
    $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
    // Closing cURL
    curl_close($ch);
    if($status_check == 1)
    {
        if($http_status != false && $http_status != 0 && $http_status != 404)
        {
            // Returning the data from the function
            return $data;
        }
    }
    else
    {
        return $data;     
    }
}

/**
 * Strips all data from before $start pos
 * Then strips the start
 * Gets the position of the $end of $data to scrape
 * Strips all data from after and including the $end of $data, to scrape
 * Returns the scraped data from the function
 * 
 * @param string $data
 * @param string $start
 * @param string $end
 * @return string
 */

function scrape_between($data, $start, $end) 
{
    
    $data = stristr($data, $start);
    
    $data = substr($data, strlen($start));

    $stop = stripos($data, $end);
    
    $data = substr($data, 0, $stop);
    
    return $data;
}

/**
 * when passing the array from google analytics, to ga_urls, 
 * it is the pages['rows'] that gets passed through
 * create an associative array with the key being the subdomain and 
 * the value being an indexed array of the urls under that subdomain
 * @param array $url_array
 * @return assoc. array
 */

function ga_urls($url_array){
    
    $subdomains_array = [];
    //run through each url returned from google
    foreach($url_array as $key => $url)
    {
        if($key ===0 || $key ===1) 
        {
            continue;
        }
        $parsed_client_list = parse_url($url[0]);

        $page_array_to_string = $parsed_client_list['path'];

        $rg_pos = strpos($page_array_to_string, 'revolutiongolf.com');

        //string position starting at .revolutiongolf.com 
        if($rg_pos === false )
        {
           continue;
        }

        $subdomain = substr($page_array_to_string, 0, $rg_pos -1);

        if(array_key_exists($subdomain, $subdomains_array))
        {
            if(in_array($page_array_to_string, $subdomains_array[$subdomain]))
            {
               continue;        
            }
        }
        $subdomain_focus = ['tourstrikertraining', 'mcleangolf'];
        if(in_array($subdomain, $subdomain_focus))
        {
            $subdomains_array[$subdomain][] = $page_array_to_string;
        }
    }
    return $subdomains_array;
}

/**
 * receives $url from foreach loop 
 * if it does not have http, prepend it
 * cURL the url and retreive the page data
 * parse the url and cature the path
 * 
 * @param string $url
 */
function create_host_dir($url, $subdomain_specific_dir) {
    $http_url = $url;
    if(strpos($url, 'http') !== 0)
    {
        $http_url = 'http:' . $url;

        if(strpos($url, '//') !== 0) 
        { 
            $http_url = 'http://' . $url;
        }
    }
    //Passing url, input by user, into the function myCurl()
    $status = true;
    
    //retrieve the index page of url and save to file in subdomain directory
    $results_page = myCurl($http_url, $status);
    
    if($results_page != '')
    {
        
        $http_url .= '.html';
        $parsed_url = parse_url($http_url);
        $directory_path = explode('/',"{$parsed_url['path']}");
        $return_array = create_dir_structure($directory_path, $results_page, $subdomain_specific_dir);
        $results_array = [$results_page, $return_array[0], $http_url];
        return $results_array;
    }
    else 
    {
        $GLOBALS['ajax_return_array']['status'] = 'failed';
        $ajax_return_array = json_encode($GLOBALS['ajax_return_array']);
        print_r($ajax_return_array);
        die();
    }
    
}

/**
 * $directory_path is an array of the url path to create the dir structure after.
 * $results_page is the scraped paged, passed to create the file.
 * $subdomain_specific_dir is the directory path to save under.
 * returns the $full_path in which it was saved - the file name
 * if it is a css file it will return an array of 
 * @param array $directory_path
 * @param string $results_page
 * @param string $subdomain_specific_dir
 * @return string (or if .css file, array)
 */
function create_dir_structure($directory_path, $results_page, $subdomain_specific_dir){
    
    $last_index = count($directory_path) - 1;
    $full_path = $subdomain_specific_dir;
    $path = '';
    
    foreach($directory_path as $key => $directory)
    {
        if($directory === '' || strpos($directory, '.com') !== false)
        {
                continue;
        }
        elseif($key < $last_index)
        {
            if(substr($path, -1) !=='/')
            {
                $path .= '/';
            }
            //these should all only be folders in the path
            //build path
            

            //foreach folder (top down)
            
            if($directory === '..')
            {
                //remove the previous directory
                $ok = $key - 1;

                $remo = $directory_path[$ok] . "/";
                $full_path = str_replace($remo, '',$full_path);

                continue;
            }

            $path .= $directory."/";

            $full_path = $subdomain_specific_dir . $path;

            //check to see if directory exists in path, if not, make it
            if(!is_dir($full_path))
            {
                //create the directory
                mkdir ($full_path, 0777);
            }
        }
        else
        {
            if(strpos($directory, '.') !== false)
            {
                //call to file creation function
                save_asset_to_file($full_path, $directory, $results_page);
                
                if(strpos($directory, '.css') !== false)
                {
                    $file_path = $full_path . $directory;
                    $results_array = [$file_path, $results_page, $path];
                    return $results_array;
                }
            }
        }
    }
    //remove the trailing / if there is one.
    if(substr($full_path, -1) ==='/')
    {
        $full_path = substr($full_path, 0, -1);
    }
    $return_array = [$full_path];
    return $return_array;
}

/**
 * 
 * @param string $full_path
 * @param string $directory
 * @param string $results_page
 */
function save_asset_to_file($full_path, $directory, $results_page){
    
    //we are expecting this to only be files that get here
    
    //remove the trailing / if there is one.
    if(substr($full_path, -1) ==='/')
    {
        $full_path = substr($full_path, 0, -1);
    }
    $file_extention_start = strrpos($directory, '.');
    
    $file_extention = substr($directory, $file_extention_start);
    
    $recorded_path = $full_path . '/' . $directory;
    
    $GLOBALS['ajax_return_array'][$file_extention][] = $recorded_path;
    $GLOBALS['ajax_return_array']['status'] = 'success';
    $f = fopen($full_path . '/' . $directory , "w");
    fwrite($f, $results_page);
    fclose($f);
    
}

/**
 * 
 * @param type $results_page
 * @return array
 */
function results_page_regex($results_page){
    $my_first_regex = 
    '/(<script|<img)[\s\w"\'\=\/\-\:\;]+src=["\']([^"\']+)["\']|<link[\s\w'
    . '"\'\=\/]+href=["\']([^"\']+)["\']|url\(["\']?([^"\'\)]+)["\'\)]?/';
    /*
     * $1 all results
     * $2 src of scripts and images
     * $3 src of css
     * $4 images in css
     */

    preg_match_all($my_first_regex, $results_page, $separate_results);
   
    return $separate_results;
}
/**
 * pass in url 
 * if it has http:// return that value
 * if it has // add http:
 * if it does not have any scheme, host or subdomain, add them and return the http_url
 * 
 * @param string $url
 * @param string $url_explode (optional)
 * @return string $http_url
 */
function add_http($url, $url_explode='', $path=''){
    
    //if not http://, add it
    $http_url = $url;
    
    
    if(strpos($url, 'http') !== 0) 
    {   
        if(strpos($url, '/') === 0 || strpos($url, '//') === 0 || strpos($url, '../') === 0)
        {
            $http_url =  'http:' . $url;
            if (strpos($url, '../') === 0)
            {
                $http_url = 'http://' . $url_explode . $path . $url;
            }
            else if(strpos($url, '//') !== 0) 
            { 
                $http_url = 'http://' . $url_explode . $url;
            }
        }
    }
  
    return $http_url;
}

/**
 * cURLs the url passed in
 * parses the url
 * explodes on the $subdomain_domain_tld['path']
 * returns an array of the cURLed results page and the directory path
 * 
 * @param string $url
 * @return array 
 */
function curl_page_explode_url($url){
    
    $results_page = myCurl($url);
    

    //remove http://subdomain.domain.tld
    $subdomain_domain_tld = parse_url($url);
    
    $results_array = [];
    if(!empty($subdomain_domain_tld))
    {    
        //break relative url on / (get folders and file)
        if(array_key_exists('path', $subdomain_domain_tld))
        {
            $dir_path = explode('/',"{$subdomain_domain_tld['path']}");
        }
        else
        {
            $dir_path = explode('/',"{$subdomain_domain_tld['host']}");
        }
        $results_array = [$results_page, $dir_path];
    } 

    return $results_array;
}



/**
 * 
 * @param string $url
 * @param array $json_array
 * @param string $target_key (visitors, opt-ins, or sales)
 * 
 * loops through to find the url that is called
 * targets and adds 1 to the value of the $target_key of that url
 *  
 * @return array
 */
function record_action($url, $json_array, $hour, $target_key) 
{   
    foreach($json_array as $key => $url_data_array) { 
        foreach($url_data_array as $subkey=>$url_data) {
            if($subkey === 'url') {
                if($url_data === $url){
                    $json_array[$key][$target_key] += 1; 
                    if($target_key === 'sale'){
                        $json_array[$key]['revenue'] += 67.00; 
                    }
                }
            }
        }
    }
    
    return $json_array;
}

/**
 * 
 * @param string $file_path 
 * 
 * gets contents from file (default file = 'results.json')
 * json decodes the file and turns it into an associative array
 * returns the array
 * 
 * @return array
 */
function get_file_array($file_path='results.json')
{
    if(!file_exists($file_path))
    {    
        $f = fopen($file_path, 'w') or die("Can't create file");
        fwrite($f, '');
        fclose($f);
        chmod($file_path, 0777);
    }
    
    $inp = file_get_contents($file_path);

    $json_array = json_decode($inp, true);
    
    return $json_array;
}

/**
 * 
 * @param array $json_array
 * @param string $file_path
 * * 
 * json encodes the associative array
 * returns contents to file (default file = 'results.json')
 * 
 */
function return_file_array($json_array, $file_path='results.json') 
{
    
    $json_data = json_encode($json_array);
    
    file_put_contents($file_path, $json_data);
}

/**
 * 
 * @param string $elem
 * @param array $array
 * @param string $field
 * 
 * $elem is the value or needle you are looking for in array ($url)
 * $field is the key you are searching in ('url')
 * and $array is the haystack
 * this helps you find if a value exists in a multidimensional array
 * 
 * @return boolean
 */
function in_multiarray($elem, $array,$field)
{
    $top = sizeof($array) - 1;
    $bottom = 0;
    while($bottom <= $top)
    {
        if($array[$bottom][$field] == $elem) {
            return $bottom;
        }
        else { 
            if(is_array($array[$bottom][$field])){
                if(in_multiarray($elem, ($array[$bottom][$field]))) {
                    return $bottom;
                }
            }
        }
        $bottom++;
    }        
    return false;
}

/**
 * 
 * uses date("G:i") to grab the current hour and minute on a 24 hour clock
 * explodes on the ':' and grabs only the hour
 * returns the hour without the leding zero
 * 
 * @return string
 */
function time_elapse(){
    
    $time = date("G:i", strtotime("7:30 PM")); //, strtotime("3:30 AM")
    
    $hour_and_minute = explode(":", $time, 2);
    
    $hour = $hour_and_minute[0];
    
    return $hour;
}

/**
 * 
 * @param array $array_24hour (.json or db array)
 * @param int/string $hour (the hour in an int or string)
 * @param array $cache_data
 * @param int/string $hours_of_data (how long you want data to be stored for)
 * 
 * recieves cache data and pushes that array onto the $hour key
 * utilizes the $hours_of_data you pass through and maintains that many hours of cache
 * returns new array of data
 * 
 * @return array $array_24hour
 */
function remove_hour($array_24hour, $hour, $hours_of_data=3) {
   
    if($hour < $hours_of_data)
    {
        if($hour == 0)
        {
            $erase_key = (count($array_24hour)-1) -2;
        }
        elseif ($hour == 1) 
        {
            $erase_key = (count($array_24hour)-1) -1;
        }
        else if ($hour == 2) 
        {
            $erase_key = (count($array_24hour)-1);
        } 
    } 
    else 
    {
        $erase_key = ($hour - $hours_of_data);
    }
    
    $array_24hour[$erase_key] = array();
    
    return $array_24hour;
}

/**
 * 
 * @param string $site
 * 
 * pass in the key/name to the sites urls (coaches, jm_urls, tst_urls, or coach_urls)
 * it will weight the site based on the opt-ins/visitor and the revenue/visitor
 * (if these values are 0, it will make them 1 for divisibility purposes)
 * if results.json file is empty, then it will add in value, just so the function doesnt fail. 
 * (will be a normal 50/50 split at that point, not a weighted one)
 * returns url to redirect to that got randomly selected (highter weighted urls will get chosen more frequently)
 * 
 * 
 * @return string 
 */
function choose_url($site) {
    
    $number = rand(1, 10000);
    
    $sites = get_file_array('redirecturls.json');

    $site_name_key = in_multiarray($site, $sites, 'name');

    $json_array = get_file_array('cache.json');

    
    $totals_array = array();
    foreach($json_array as $key=>$hour_array) {
     
        if(empty($hour_array)) 
        {
            continue;
        }
        foreach($hour_array as $url_array) {
            if(empty($url_array)) 
            {
                continue;
            }
            $url = '';
            foreach($url_array as $k=> $value) 
            {
                if($k == 'url')
                {
                    $url = $value;
                }
                
                $bool = in_multiarray($url, $totals_array, 'url');
                if($bool || $bool === 0) 
                {
                    if($k == 'url')
                    {
                        $totals_array[$bool][$k] = $value;
                    } 
                    else 
                    {
                        $totals_array[$bool][$k] += $value;
                    }
                }
                else 
                {
                    $totals_array[] = array(
                        'url' => $url,
                        'visitor' => 0,
                        'opt-in' => 0,
                        'sale' => 0,
                        'revenue' => 0
                    );
                }
                
            }
        }
    }
    
    $json_array_key = array();
    $i = 0;
    foreach($sites[$site_name_key]['urls'] as $url) 
    {   
        $bool = in_multiarray($url, $totals_array,'url');
        if($bool || $bool === 0) 
        {
            $json_array_key[] = $bool;
        } 
        else 
        {
            $totals_array[] = array(
                'url' => $url,
                'visitor' => 1,
                'opt-in' => 0,
                'sale' => 0,
                'revenue' => 0
            );
            $json_array_key[] = $i;
        }
        $i++;
    }

    $pos_1_step_1 = $totals_array[$json_array_key[0]];
    $pos_2_step_1 = $totals_array[$json_array_key[1]];
    $pos_3_step_1 = $totals_array[$json_array_key[2]];
    $pos_4_step_1 = $totals_array[$json_array_key[3]];
    
    $pos_1_optin = $pos_1_step_1['opt-in'];
    if($pos_1_optin === 0) {
        $pos_1_optin +=1;
    }
    $pos_2_optin = $pos_2_step_1['opt-in'];
    if($pos_2_optin === 0) {
        $pos_2_optin +=1;
    }
    $pos_3_optin = $pos_3_step_1['opt-in'];
    if($pos_3_optin === 0) {
        $pos_3_optin +=1;
    }
    $pos_4_optin = $pos_4_step_1['opt-in'];
    if($pos_4_optin === 0) {
        $pos_4_optin +=1;
    }
    $pos_1_revenue = $pos_1_step_1['revenue'];
    if($pos_1_revenue === 0) {
        $pos_1_revenue +=1;
    }
    $pos_2_revenue = $pos_2_step_1['revenue'];
    if($pos_2_revenue === 0) {
        $pos_2_revenue +=1;
    }
    $pos_3_revenue = $pos_3_step_1['revenue'];
    if($pos_3_revenue === 0) {
        $pos_3_revenue +=1;
    }
    $pos_4_revenue = $pos_4_step_1['revenue'];
    if($pos_4_revenue === 0) {
        $pos_4_revenue +=1;
    }
    $pos_1_step_2 = (($pos_1_optin/$pos_1_step_1['visitor']) * 10)+($pos_1_revenue/$pos_1_step_1['visitor']);
    $pos_2_step_2 = (($pos_2_optin/$pos_2_step_1['visitor']) * 10)+($pos_2_revenue/$pos_2_step_1['visitor']);
    $pos_3_step_2 = (($pos_3_optin/$pos_3_step_1['visitor']) * 10)+($pos_3_revenue/$pos_3_step_1['visitor']);
    $pos_4_step_2 = (($pos_4_optin/$pos_4_step_1['visitor']) * 10)+($pos_4_revenue/$pos_4_step_1['visitor']);

    $step_2_total = ($pos_1_step_2 + $pos_2_step_2 + $pos_3_step_2 + $pos_4_step_2);
    
    $pos_1 = round((($pos_1_step_2/$step_2_total) *10000), 0);

    $pos_2 = round((($pos_2_step_2/$step_2_total) *10000), 0);

    $pos_3 = round((($pos_3_step_2/$step_2_total) *10000), 0);

    $pos_4 = round((($pos_4_step_2/$step_2_total) *10000), 0);

    $organized_array = array(array($pos_1, $pos_1_step_1['url']), array($pos_2, $pos_2_step_1['url']), array($pos_3, $pos_3_step_1['url']), array($pos_4, $pos_4_step_1['url']));

    rsort($organized_array);

    $posibility_1 = (10000-$organized_array[0][0]);
    $posibility_2 = ($posibility_1 - $organized_array[1][0]);
    $posibility_3 = ($posibility_2 - $organized_array[2][0]);

    if($number >= $posibility_1){
            //redirect to this page
            return $organized_array[0][1];
        }
        elseif($number >= $posibility_2){
            //redirect here
            return $organized_array[1][1];
            
        }
        elseif($number >= $posibility_3) {
            //redirect here
            return $organized_array[2][1];
        }
        else {
            //redirect here
            return $organized_array[3][1];
        }
}

/**
 * 
 * @param string $full_path
 * @param string $results_page
 */
function save_to_file_if_no_errors($full_path, $results_page){
    $strpos = strpos($results_page, "<div");
    if($strpos !== FALSE){
        $f = fopen($full_path, "w");
        fwrite($f, $results_page);
        fclose($f);
        echo "  $full_path was successful  ";
    }
    else
    {
        echo "  $full_path failed...error was thrown.  ";
    }
}

class DB {
	
	// Singleton Instance
	private static $instance;
	private $link;

	/**
	 * Connect to MySQL database
	 */
	public function __construct() {
		try {
			$this->link = new PDO('mysql:host=' . DB_HOST .';dbname=' . DB_NAME . ';charset=utf8', DB_USER, DB_PASS);
			$this->link->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			$this->link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	/****************************************
	  Singleton
	*****************************************/

	public static function init() {

		// Singleton
		if (!isset(self::$instance)) {
			$class = __CLASS__;
			self::$instance = new $class();
		}

		// Return Instance
		return self::$instance->link;

	}

	/****************************************
	  Execute
	*****************************************/
	
	public static function execute($statement, $sql_values = []) {
		try {
			$link = self::init();
			if (is_array($sql_values)) {
				$statement->execute($sql_values);
			} else {
				$statement->execute();
			}
			return $statement;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	/****************************************
	  Prepare
	*****************************************/

	public static function prepare($sql) {
		try {
			$link = self::init();
			$statement = $link->prepare($sql);
			return $statement;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	/****************************************
	  Last Insert ID
	*****************************************/

	public static function lastInsertId() {
		$link = self::init();
		return $link->lastInsertId();
	}

}

function addUser() {
//    require_once('initialize.php');

$first_name = '';
$last_name = '';
$email = '';

if(isset($_POST['first_name'])) {
    $first_name = $_POST['first_name'];
}

if(isset($_POST['last_name'])) {
    $last_name = $_POST['last_name'];
}

if(isset($_POST['email'])) {
    $email = $_POST['email'];
}

function add_user($first_name, $last_name, $email) {
    
    $insert = 'insert into user (first_name, last_name, email, subscribed, permissions) values (:first_name, :last_name, :email, NOW()), user';
    
    $ins_stmt = DB::prepare($insert);
	
    DB::execute($ins_stmt,[':first_name' => $first_name, ':last_name' => $last_name, ':email' => $email]);
   
    
}

add_user($first_name, $last_name, $email);
header('Location: index.php');
exit();
}

function schemaReference(){
    
//DROP DATABASE IF EXISTS contourthis;
//CREATE DATABASE contourthis;
//USE contourthis;
//
//
//CREATE TABLE user (
//    id INT AUTO_INCREMENT PRIMARY KEY,
//    first_name VARCHAR(50),
//    last_name VARCHAR(50),
//    email VARCHAR(255),
//    subscribed TIMESTAMP,
//    permissions VARCHAR(50)
//);

}

/**
 * A class to handle secure encryption and decryption of arbitrary data
 *
 * Note that this is not just straight encryption.  It also has a few other
 *  features in it to make the encrypted data far more secure.  Note that any
 *  other implementations used to decrypt data will have to do the same exact
 *  operations.  
 *
 * Security Benefits:
 *
 * - Uses Key stretching
 * - Hides the Initialization Vector
 * - Does HMAC verification of source data
 *
 */
class Encryption {

    /**
     * @var string $cipher The mcrypt cipher to use for this instance
     */
    protected $cipher = '';

    /**
     * @var int $mode The mcrypt cipher mode to use
     */
    protected $mode = '';

    /**
     * @var int $rounds The number of rounds to feed into PBKDF2 for key generation
     */
    protected $rounds = 100;

    /**
     * Constructor!
     *
     * @param string $cipher The MCRYPT_* cypher to use for this instance
     * @param int    $mode   The MCRYPT_MODE_* mode to use for this instance
     * @param int    $rounds The number of PBKDF2 rounds to do on the key
     */
    public function __construct($cipher, $mode, $rounds = 100) {
        $this->cipher = $cipher;
        $this->mode = $mode;
        $this->rounds = (int) $rounds;
    }

    /**
     * Decrypt the data with the provided key
     *
     * @param string $data The encrypted datat to decrypt
     * @param string $key  The key to use for decryption
     * 
     * @returns string|false The returned string if decryption is successful
     *                           false if it is not
     */
    public function decrypt($data, $key) {
        $salt = substr($data, 0, 128);
        $enc = substr($data, 128, -64);
        $mac = substr($data, -64);

        list ($cipherKey, $macKey, $iv) = $this->getKeys($salt, $key);

        if (!hash_equals(hash_hmac('sha512', $enc, $macKey, true), $mac)) {
             return false;
        }

        $dec = mcrypt_decrypt($this->cipher, $cipherKey, $enc, $this->mode, $iv);

        $data = $this->unpad($dec);

        return $data;
    }

    /**
     * Encrypt the supplied data using the supplied key
     * 
     * @param string $data The data to encrypt
     * @param string $key  The key to encrypt with
     *
     * @returns string The encrypted data
     */
    public function encrypt($data, $key) {
        $salt = mcrypt_create_iv(128, MCRYPT_DEV_URANDOM);
        list ($cipherKey, $macKey, $iv) = $this->getKeys($salt, $key);

        $data = $this->pad($data);

        $enc = mcrypt_encrypt($this->cipher, $cipherKey, $data, $this->mode, $iv);

        $mac = hash_hmac('sha512', $enc, $macKey, true);
        return $salt . $enc . $mac;
    }

    /**
     * Generates a set of keys given a random salt and a master key
     *
     * @param string $salt A random string to change the keys each encryption
     * @param string $key  The supplied key to encrypt with
     *
     * @returns array An array of keys (a cipher key, a mac key, and a IV)
     */
    protected function getKeys($salt, $key) {
        $ivSize = mcrypt_get_iv_size($this->cipher, $this->mode);
        $keySize = mcrypt_get_key_size($this->cipher, $this->mode);
        $length = 2 * $keySize + $ivSize;

        $key = $this->pbkdf2('sha512', $key, $salt, $this->rounds, $length);

        $cipherKey = substr($key, 0, $keySize);
        $macKey = substr($key, $keySize, $keySize);
        $iv = substr($key, 2 * $keySize);
        return array($cipherKey, $macKey, $iv);
    }

    /**
     * Stretch the key using the PBKDF2 algorithm
     *
     * @see http://en.wikipedia.org/wiki/PBKDF2
     *
     * @param string $algo   The algorithm to use
     * @param string $key    The key to stretch
     * @param string $salt   A random salt
     * @param int    $rounds The number of rounds to derive
     * @param int    $length The length of the output key
     *
     * @returns string The derived key.
     */
    protected function pbkdf2($algo, $key, $salt, $rounds, $length) {
        $size   = strlen(hash($algo, '', true));
        $len    = ceil($length / $size);
        $result = '';
        for ($i = 1; $i <= $len; $i++) {
            $tmp = hash_hmac($algo, $salt . pack('N', $i), $key, true);
            $res = $tmp;
            for ($j = 1; $j < $rounds; $j++) {
                 $tmp  = hash_hmac($algo, $tmp, $key, true);
                 $res ^= $tmp;
            }
            $result .= $res;
        }
        return substr($result, 0, $length);
    }

    protected function pad($data) {
        $length = mcrypt_get_block_size($this->cipher, $this->mode);
        $padAmount = $length - strlen($data) % $length;
        if ($padAmount == 0) {
            $padAmount = $length;
        }
        return $data . str_repeat(chr($padAmount), $padAmount);
    }

    protected function unpad($data) {
        $length = mcrypt_get_block_size($this->cipher, $this->mode);
        $last = ord($data[strlen($data) - 1]);
        if ($last > $length) return false;
        if (substr($data, -1 * $last) !== str_repeat(chr($last), $last)) {
            return false;
        }
        return substr($data, 0, -1 * $last);
    }
}