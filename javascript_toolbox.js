//jquery replacement
var $ = function ( elem ) {
    return document.querySelectorAll( elem );
    // Usage
    //var myClass = $('.myClass');
};

 var setTimeoutDelux = {
        execute: function() {
            console.log('does something you want to happen only once in the aloted time');
            delete this.timeoutID;
        },

        setup: function() {
            this.cancel();
            var self = this;
            this.timeoutID = window.setTimeout(function() {self.execute();}, 2000);
        },

        cancel: function() {
            if(typeof this.timeoutID == "number") {
                window.clearTimeout(this.timeoutID);
                delete this.timeoutID;
            }
        }
    };
    
    
+function(jQuery){
        var scrollStopEventEmitter = function(element, jQuery) {
            this.scrollTimeOut = false;
            this.element       = element;
            jQuery(element).on('scroll', $.proxy(this.onScroll, this));
        }

        scrollStopEventEmitter.prototype.onScroll = function() 
        {
            if (this.scrollTimeOut != false) {
              clearTimeout(this.scrollTimeOut);
            }

            var context = this;

            this.scrollTimeOut = setTimeout(function(){ context.onScrollStop()}, 200);
        }

        scrollStopEventEmitter.prototype.onScrollStop = function() 
        {
            this.element.trigger('scrollStop');
        }

        jQuery.fn.scrollStopEventEmitter = function(jQuery) {   
            return new scrollStopEventEmitter(this, jQuery);
        };

    }($);

function containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i].key === obj.key) {
            return i;
        }
    }

    return -1;
}

function sortArray(valuesOrdered) {
    valuesOrdered.sort(function(a, b) {

        if (a.value > b.value) {
            return 1;
        }
        if (a.value < b.value) {
            return -1;
        }
        // a must be equal to b
        return 0;
    });
}

function magneticScroll(pixels, direction) {
    // console.log('magnetic scroll scrolling');
    if(userScroll) {
        previousScroll = $(window).scrollTop() + $(window).height();
        return;
    }
    
    if( direction === 'up') {
        if( pixels < 0){
        window.scrollBy(0, -1);
        magneticScrollTimeout = setTimeout(function(){
           magneticScroll(pixels+1, 'up'); 
            },scrollSpeed);
        }
        else {
            previousScroll = $(window).scrollTop() + $(window).height();
            return;
        }
    }
    else {
        if( pixels > 0){
        window.scrollBy(0,1);
        magneticScrollTimeout = setTimeout(function(){
           magneticScroll(pixels-1, 'down'); 
            },scrollSpeed);
        }
        else {
            previousScroll = $(window).scrollTop() + $(window).height();
            return;
        }
    }
}

function orderDomElements(newElements) {

    var elements = document.querySelectorAll(selector)

    if(newElements === true) 
    {
        for(var i=0; i < 100; i++) 
        {

            $('.msjse'+i).removeClass('msjse'+i);
        }
    }
    console.dirxml(elements);
    //this keeps track of the document if the forever scroll functionality is activated on a page
    previousDocumentHeight = document.body.scrollHeight;

    //ordered array for pixel distance from top or bottom of element to top of document
    valuesOrdered = [];

    //stores top and bottom of each element grabbed by selector
    valuesTopBottom = {};

    //iterates over each element in the DOM 
    for (var i=0, max=elements.length; i < max; i++) 
    {

        try{

            //current DOM element
            var object = $(elements[i]);
            /*height doesn't include margin, padding and border because
            that could be visualized as white space*/
            var height = object.height();
            if(height >= 0 && height <= $(window).height()) {
                object.addClass('msjse'+i);
                console.dirxml(object);
                var scrollTop = $(window).scrollTop();
                var top = $('.msjse'+i).offset().top;
                // var topDistanceFromTop = (top - scrollTop);
                var objTop = {key: 'msjse'+i, value: top}; //topDistanceFromTop};
                valuesOrdered.push(objTop);
                var bottom = top+height;
                // var bottomDistanceFromTop = (bottom - scrollTop);
                var objBottom = {key: 'msjse'+i, value: bottom}; //bottomDistanceFromTop};
                valuesOrdered.push(objBottom);
                valuesTopBottom['msjse'+i] = {top: top, bottom: bottom }; //topDistanceFromTop , bottomDistanceFromTop
            }
        }
        catch(err)
        {
            console.log('error ');
            console.log(err);
        }
    }
}

function determineScrollDirection(bottomViewPortFromTop, after, keyofValues) {
    var direction = '';

    var body = document.body,
    html = document.documentElement;

    var height = Math.max( body.scrollHeight, body.offsetHeight, 
    html.clientHeight, html.scrollHeight, html.offsetHeight );
    if(bottomViewPortFromTop > previousScroll || previousScroll === 0) 
    {
        if(bottomViewPortFromTop >= (height - 50) || typeof(after) === 'undefined') 
        {
            console.log('you have reached the bottom of the page');
            previousScroll = bottomViewPortFromTop;
            direction = 'false';
        } 
        else 
        {
            if(activatedDirection === 'down' || activatedDirection === 'both'){
                console.log('scrolling down');
                direction = 'down';
            }
            else
            {
                direction = 'false';
            }
        }
    }
    else if(bottomViewPortFromTop < previousScroll) 
    {
        if(bottomViewPortFromTop >= (height - 50) || typeof(after) === 'undefined') 
        {
            console.log('you have reached the bottom of the page');
            previousScroll = bottomViewPortFromTop;
            direction = 'false';
        } 
        else 
        {
            if(activatedDirection === 'up' || activatedDirection === 'both'){
                console.log('scrolling up');
                direction = 'up';
            }
            else
            {
                direction = 'false';
            }
        }
    }
    else 
    {
        //if user scrolls up then back down ending in the same position visa versa down then up
        console.log('didnt move');
        previousScroll = bottomViewPortFromTop;
        direction = 'false';
    }
    return direction;
};


function magneticScrollCustom(selector, scrollInto, activatedDirection, TurnOffMScrollAtEOP, scrollSpeed, maxScroll) {
    // :not is required if grabbing everything, not if user is passing in a specific selector (ie: element, class, etc).
    if(typeof(selector) === 'undefined' || typeof(selector) !== 'string')
    {
        var selector = '*:not(script, style, noscript, head, meta, title, span)';   
    }

    // % to scroll into div (img, div, paragraph, etc).
    if(typeof(scrollInto) === 'undefined')
    {
        var scrollInto = 30/100;
    }
    else 
    {
        scrollInto = scrollInto/100;
    }

    // time in miliseconds.
    if(typeof(scrollSpeed) === 'undefined' || scrollSpeed > 11 || scrollSpeed < 0 )
    {
        var scrollSpeed = 5; 
    }
    else 
    {
        if(scrollSpeed === 'fast')
        {
            scrollSpeed = 1;
        }
        else if(scrollSpeed === 'slow')
        {
            scrollSpeed = 10;
        }
    }
    console.log('scrollSpeed');
    console.log(scrollSpeed);

    // % of max scroll. Usually the % of the view port you would like to scroll (ie: 1-30% of viewport). can be any percent.
    if(typeof(maxScroll) === 'undefined'|| typeof(maxScroll) !== 'number')
    {
        var maxScroll = 40/100;
    }
    else
    {
        maxScroll = maxScroll/100;
    }

    //direction developer chooses to have magnetic scroll activated on. default is both.
    if(typeof(activatedDirection) === 'undefined')
    {
        var activatedDirection = 'both'; 
    }
    if(typeof(TurnOffMScrollAtEOP) === 'undefined' || typeof(TurnOffMScrollAtEOP) !== "boolean")
    {
        var TurnOffMScrollAtEOP = false; 
    }

    //convert to global variables to track if the user scrolls up or down, and to manage developer preferences
    $(window).scrollStopEventEmitter($); //instantiate the scrollStop event and bind it to the window
    previousScroll = 0;
    msEOP = false;
    newElements = false;
    window.selector = selector;
    window.scrollSpeed = scrollSpeed;
    window.maxScroll = maxScroll;
    window.scrollInto = scrollInto;
    window.activatedDirection = activatedDirection;
    window.TurnOffMScrollAtEOP = TurnOffMScrollAtEOP;

    //call ms function to initiate the scroll event listener.
    ms();
};

function ms() {
    if(typeof(selector)==='undefined'){return;} 
    if(typeof(scrollSpeed)==='undefined'){return;}
    if(typeof(maxScroll)==='undefined'){return;}
    if(typeof(scrollInto)==='undefined'){return;}

    //grabs every element in the DOM.
    // var all = $(selector);
    
    //iterates over each element in the DOM . newElements starts as false
    orderDomElements(newElements);

    //sort the values ordered array.
    sortArray(valuesOrdered);

    var reEvaluatePage = {
        updatePage: function() {
            console.log('evaluating page for new divs');
            newElements = true;
            orderDomElements(newElements);
            sortArray(valuesOrdered);
            // ms();

            delete this.timeoutID;
        },

        setup: function() {
            this.cancel();
            var self = this;
            this.timeoutID = window.setTimeout(function() {self.updatePage();}, 2000);
        },

        cancel: function() {
            if(typeof this.timeoutID == "number") {
                window.clearTimeout(this.timeoutID);
                delete this.timeoutID;
            }
        }
    };
    
    var timer;

    //default scrolling to false, so the first scroll can be seen as the user.
    scrolling = false;
    userScroll = false;
    magneticScrollTimeout = 0;
    var page = $(document);
    page.bind('scroll mousedown wheel DOMMouseScroll mousewheel keydown touchmove', function(e){
        
        //if it is user scrolling, then
        if ( e.which > 0 || e.type == "mousedown" || e.type == "mousewheel" || e.type == "touchmove" || e.type == "keydown" || e.type == "DOMMouseScroll")
        {
            window.clearTimeout(magneticScrollTimeout);
            previousScroll = $(window).scrollTop() + $(window).height(); 
        }

        var currentDocumentHeight = document.body.scrollHeight;
        if(currentDocumentHeight > previousDocumentHeight)
        {

            document.addEventListener("DOMNodeInserted", function(e) {
                event_target = $(e.target);
                var classList = e.target.className;
                if(classList.indexOf(selector) > -1) {
                    var element = e.target;
                }
                else {
                    var element = child.children(selector);
                }
                    
              valuesOrdered.push(element);
              
            }, false);
            
            console.log('currentDocumentHeight');
            console.log(currentDocumentHeight);
            console.log('previousDocumentHeight');
            console.log(previousDocumentHeight);
            previousDocumentHeight = currentDocumentHeight;
            reEvaluatePage.setup();
            // setTimeout(function(){
            // // orderDomElements($(selector));
            // // sortArray(valuesOrdered);
            // console.log('grab new divs');
            // previousDocumentHeight = currentDocumentHeight;
            // }, 1000);
        }

        //makes sure the function refresh isnt running twice.
        // clearTimeout(timer);

        //prevents against window.scroll triggering refresh function.
        if (scrolling)
        {   
            //call the refresh function after alloted time to simulate end of scroll. having this at 5 feels like you accidentally scrolled to far. 30 best?
            // timer = setTimeout( refresh, 50 );
            previousScroll = $(window).scrollTop() + $(window).height();
        }
    });

$(window).on('scrollStop',function()
{
    if(msEOP === true)
    {
        return;
    }
    console.log('done scrolling');
    if (!scrolling)
        {   
            //call the refresh function after alloted time to simulate end of scroll. having this at 5 feels like you accidentally scrolled to far. 30 best?
            // timer = setTimeout( refresh, 50 );
            refresh();
        }
        else 
        {
            //if the scroll function is in effect (ie: not the user), then it adds the new current position to the previous scroll.
            previousScroll = $(window).scrollTop() + $(window).height();
        }
});


    /**
    step one. track scroll event and wait alotted time before calling scroll function to simulate end of scroll
    jQuery special event (uses unevent to run)
    */
    // $(window).bind('scroll',function () {

    //     clearTimeout(timer);

    //     //prevents against window.scroll triggering refresh function
    //     if (!scrolling)
    //     {   
    //         //call the refresh function after alloted time to simulate end of scroll. having this at 5 feels like you accidentally scrolled to far. 30 best?
    //         timer = setTimeout( refresh, 30 );
    //     }
    //     else 
    //     {
    //         //if the scroll function is in effect (ie: not the user), then it adds the new current position to the previous scroll
    //         previousScroll = $(window).scrollTop() + $(window).height();
    //     }
    // });
};

function refresh() {
    console.log('refresh called');
    
    var keyofValues;

    //the bottom of the users screen.
    var bottomViewPortFromTop = $(window).scrollTop() + $(window).height();

    //viewport object to put into array.
    var viewportObj = {key: "viewPort", value: bottomViewPortFromTop};

    //push pbject into array.
    valuesOrdered.push(viewportObj);
    console.log('viewport added to array');

    //sort the array to find the location of the viewport based on other elements.
    sortArray(valuesOrdered);

    //find the key where the viewport object resides in the new sorted array.
    keyofValues = containsObject(viewportObj, valuesOrdered);

    //find the element before the bottom of our viewport location.
    var before = valuesOrdered[keyofValues -1];

    //find the element after the bottom of our viewport location.
    var after = valuesOrdered[keyofValues +1];
    if(typeof(after)==='undefined')
    {
        valuesOrdered.splice(keyofValues,1);
        console.log('viewport removed from array');
        if(TurnOffMScrollAtEOP === true){
            msEOP = true;
        }
        return;
    } 
    else if (after.key === "viewPort") 
    {
        valuesOrdered.splice(keyofValues,2);
        console.log('viewport removed from array');
        return;
    }

    var direction = determineScrollDirection(bottomViewPortFromTop, after, keyofValues);
    
    //either hit the bottom of page or didnt move.
    if(direction === 'false')
    {
        valuesOrdered.splice(keyofValues,1);
        console.log('viewport removed from array');
        return;
    }

    var beforeElement = valuesTopBottom[before.key];
    
    var afterElement = valuesTopBottom[after.key];

    var previousElement;
    var previousElementXY;
    for(i=2; i<10; i++) 
    {
        previousElement = valuesOrdered[keyofValues -i];
        previousElementXY = valuesTopBottom[previousElement.key];
        if(beforeElement.bottom !== previousElementXY.bottom)
        {
            break;
        }  
    }
     var nextElement;
     var nextElementXY;
    for(i=2; i<10; i++) 
    {
        nextElement = valuesOrdered[keyofValues +i];
        if(nextElement === undefined){    //saying that the after object is undefined every rotation and its going in reverse until it actually is undefined but it is skipping past the typeof check...
            nextElement = valuesOrdered[keyofValues + (i - 1)];
            nextElementXY = valuesTopBottom[nextElement.key];
            break;
        } 
        nextElementXY = valuesTopBottom[nextElement.key];
        if(nextElementXY===undefined) 
        {
            nextElement = valuesOrdered[keyofValues + (i - 1)];
            break;
        }
    }
    if (direction === 'down') 
    {
        var elementHeight = afterElement.bottom - afterElement.top;
        // var parentElementHeight = previousElementXY.bottom - previousElementXY.top;
    }
    else if (direction === 'up') 
    {
        var elementHeight = beforeElement.bottom - beforeElement.top;
        // var parentElementHeight = nextElementXY.bottom - nextElementXY.top;
    }
    
    if( beforeElement.top < bottomViewPortFromTop && beforeElement.bottom > bottomViewPortFromTop) //|| afterElement.bottom > bottomViewPortFromTop && afterElement.top < bottomViewPortFromTop 
    {
        console.log('inside element');
        // inside element
        var userOffset = elementHeight * scrollInto;
        userOffset = Math.round(userOffset);

        if((beforeElement.bottom - bottomViewPortFromTop) < userOffset ) // (1400 - 1350) < 90  === true 
        {
            //IF USER LANDS ON BOTTOM OF IMAGE.

            if(direction === 'down')
            {
                var scroll = (nextElementXY.top - bottomViewPortFromTop) + userOffset; // (1500 - 1350) + 90 === down 240px
            }
            else 
            {
                var scroll = (beforeElement.bottom - bottomViewPortFromTop) - userOffset;  // (1400 - 1350) -90 === up 40px ( -40px )
            }
        }
        else if((bottomViewPortFromTop - beforeElement.top) < userOffset ) // (1350 - 1300) < 90 === true
        {
           //if user lands on the top of an image.

            if(direction === 'down') 
            {
                var scroll = ((beforeElement.top - bottomViewPortFromTop) + userOffset); //(1300 - 1350) + 90 = down 40px
            } 
            else 
            {
                //up
                var scroll = ((previousElementXY.bottom - bottomViewPortFromTop) - userOffset); // (1200 -1350) -90 = up 240px (-240px)
            }
        }

        var dontScrollPast = Math.round($(window).height() * maxScroll);
        if(scroll > dontScrollPast)
        {
            scroll = dontScrollPast;
        }  

        magneticScroll(scroll, direction);
        setTimeout(function(){
            scrolling = false;
            }, 500);
        scrolling = true;        
    }
    // else if(afterElement.bottom > bottomViewPortFromTop && afterElement.top < bottomViewPortFromTop)
    // {
    //     //inside element

    // }
    else  
    {
        //in whitespace
        var userOffset = elementHeight * scrollInto;
        console.log(elementHeight);
        userOffset = Math.round(userOffset);
        console.log(userOffset);
        var dontScrollPast = Math.round($(window).height() * maxScroll);
        console.log(dontScrollPast);

        if(bottomViewPortFromTop > previousScroll) 
        {
            // scrolling down
            var scroll =((afterElement.top - bottomViewPortFromTop) + userOffset);
        }
        else 
        {
            //scrolling up
            var scroll = ((beforeElement.bottom - bottomViewPortFromTop) - userOffset);
        }

        if(scroll > dontScrollPast)
        {
            scroll = dontScrollPast;
        }        
        magneticScroll(scroll, direction);
            setTimeout(function(){
            scrolling = false;
            }, 500);
            scrolling = true;

    }

    previousScroll = $(window).scrollTop() + $(window).height();
    valuesOrdered.splice(keyofValues,1); 
    console.log('viewport removed from array');
};
